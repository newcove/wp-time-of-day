<?php
/*
* Plugin Name: Time of Day
* Description: Output morning, afternoon or evening depending on the time of day.
* Version: 1.0
* Author: New Cove
* Author URI: https://newcove.co.uk
*/


function nc_time_of_day(){
    $current_hour = date("H");
    if($current_hour > 17) {
        return 'evening';
    } elseif($current_hour > 11) {
        return 'afternoon';
    } else {
        return 'morning';
    }
}
add_shortcode('timeofday', 'nc_time_of_day');
?>